# DSMEN (Design System du Ministère de l'Education Nationale)

Il n'y a pas, à proprement parler, de _design system_ spécifique au MENJS. Il y a des initiatives qui s'inscrivent dans le sillage du [Système de Design de l'État Français (DSFR)](https://www.systeme-de-design.gouv.fr/). 

Ces initiatives sont de différentes natures.

## Portages vers des technologies ciblées

Ces initiatives poursuivent toutes le même objectif : faciliter l'intégration des matériaux produits par l'équipe produit DSFR au sein de diverses technologies utilisées au sein du MENJS.

### Angular

### PHP/Twig

### Drupal

### Web Components

## Extensions au DSFR

Cette initiative, conforme au cadre d'usage imposé par les [CGU DSFR](https://www.systeme-de-design.gouv.fr/a-propos/conditions-generales-d-utilisation) consiste à élaborer des composants qui n'existent pas (encore) du côté du DSFR.

Ces composants ont été construits en cohérence avec les composants déjà existants, dans le respect des fondamentaux techniques, des éléments d'identité, de l'esprit et des principes de design du DSFR, ceci pour répondre à des besoins fonctionnels non couverts par le DSFR.


